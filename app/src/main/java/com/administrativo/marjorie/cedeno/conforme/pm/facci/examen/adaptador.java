package com.administrativo.marjorie.cedeno.conforme.pm.facci.examen;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class adaptador  extends RecyclerView.Adapter<adaptador.MyViewHolder> {
    private ArrayList<administrativo> administrativo;


    public adaptador(ArrayList<administrativo> administrativo) {
        this.administrativo = administrativo;
    }

    @NonNull
    @Override
    public adaptador.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull adaptador.MyViewHolder myViewHolder, int position) {
        administrativo administrativo1 = administrativo.get(position);
        myViewHolder.id.setText(administrativo1.getId());
        myViewHolder.nombre.setText(administrativo1.getId());
        myViewHolder.apellido.setText(administrativo1.getApellido());
        myViewHolder.cargo.setText(administrativo1.getCargo());
        myViewHolder.nombramiento.setText(administrativo1.getNombramiento());
        Picasso.get().load(administrativo1.getFoto()).into(myViewHolder.foto);

    }

    @Override
    public int getItemCount() {

        class MyViewHolder {
            private ImageView foto;
            private TextView id, nombre, apellido, cargo, nombramiento;

            public MyViewHolder(@NonNull View itemView) {

                foto = (ImageView) itemView.findViewById(R.id.foto);
                id = (TextView) itemView.findViewById(R.id.id);
                nombre = (TextView) itemView.findViewById(R.id.nombre);
                apellido = (TextView) itemView.findViewById(R.id.nombramiento);
                cargo = (TextView) itemView.findViewById(R.id.cargo);
                nombramiento = (TextView) itemView.findViewById(R.id.nombramiento);

            }
        }


    }

