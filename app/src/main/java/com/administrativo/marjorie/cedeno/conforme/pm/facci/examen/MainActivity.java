package com.administrativo.marjorie.cedeno.conforme.pm.facci.examen;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private static final String URL_admin = "http://10.1.15.127:3003/administrativos";
    private RecyclerView.LayoutManager layoutManager;
    private adaptador adaptador;
    private ArrayList<administrativo> arrayListP;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.administrativos);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        arrayListP = new ArrayList<>();
        adaptador = new adaptador(arrayListP);
        progressDialog = new ProgressDialog(this);

        ObtenerPersonal();
    }

    private void ObtenerPersonal() {
        progressDialog.setCancelable(false);
        progressDialog.show();
            final StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_admin, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Response", response);
                    progressDialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            administrativo administrativo = new administrativo();
                            administrativo.setFoto(jsonObject.getString("foto"));
                            administrativo.setId(jsonObject.getString("id"));
                            administrativo.setNombre(jsonObject.getString("apellido"));
                            administrativo.setApellido(jsonObject.getString("apellido"));
                            administrativo.setCargo(jsonObject.getString("profesion"));
                            administrativo.setNombramiento(jsonObject.getString("nombramiento"));
                            arrayListP.add(administrativo);

                        }
                        recyclerView.setAdapter(adaptador);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
                }

            }